var http = require("http");

var streznik = http.createServer(function(zahteva, odgovor) {
  odgovor.writeHead(200, {"Content-Type": "text/html"});
  odgovor.write("<h1>Lep pozdrav</h1><p>Zahteval si <code>" + zahteva.url + "</code></p>");
  odgovor.end();
});

streznik.listen(8000, function() {
  console.log("Strežnik pognan.");
});
