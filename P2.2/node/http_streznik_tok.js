var http = require("http");

http.createServer(function(zahteva, odgovor) {
  odgovor.writeHead(200, {"Content-Type": "text/html"});
  zahteva.on("data", function(delcek) {
    odgovor.write(delcek.toString().toUpperCase());
  });
  zahteva.on("end", function() {
    odgovor.end();
  });
}).listen(8000, function() {
  console.log("Strežnik pognan.");
});
