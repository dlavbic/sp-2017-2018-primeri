var figlet = require("figlet");

figlet.text("Spletno programiranje", function(napaka, podatki) {
  if (napaka)
    console.error(napaka);
  else
    console.log(podatki);
});
